PRODUCT_PACKAGES += \
    PixelBuiltInPrintService \
    CaptivePortalLoginOverlay \
    CellBroadcastReceiverOverlay \
    CellBroadcastServiceOverlay \
    PixelContactsProvider \
    GoogleConfigOverlay \
    GooglePermissionControllerOverlay \
    GoogleWebViewOverlay \
    ManagedProvisioningPixelOverlay \
    NetworkStackOverlay \
    PixelConfigOverlay2018 \
    PixelConfigOverlay2019 \
    PixelConfigOverlay2019Midyear \
    PixelConfigOverlay2021 \
    PixelConfigOverlayCommon \
    PixelConnectivityOverlay2021 \
    PixelSetupWizardOverlay \
    PixelSetupWizardOverlay2019 \
    PixelSetupWizard_rro \
    PixelTetheringOverlay2021 \
    PixelSettingsGoogle \
    PixelSettingsProvider \
    SystemUIGXOverlay \
    PixelSystemUIGoogle \
    PixelTeleService \
    PixelTelecom \
    Pixelframework-res \
